﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMover : MonoBehaviour {

	public Vector3 speedySpeed;
	private Rigidbody rb;
	void Start ()
	{
		rb = GetComponent<Rigidbody>();
		rb.velocity = speedySpeed;
	}
}
