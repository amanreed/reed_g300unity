﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cloudSpawner : MonoBehaviour {
	public GameObject cloud;
	public Vector3 spawnValues; 
	public int cloudCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;
	public Vector3 rotater; 



	void Start ()
	{
		StartCoroutine (SpawnClouds ());
	}




	IEnumerator SpawnClouds (){
		Debug.Log("SpawnClouds Called");
		yield return new WaitForSeconds (startWait);
		while (true)
		{
			for (int i = 0; i < cloudCount; i++) 
			{	

				Vector3 spawnPosition = new Vector3 (spawnValues.x, spawnValues.y,Random.Range (-spawnValues.z, spawnValues.z));
				Quaternion spawnRotation = Quaternion.Euler(rotater); 
				Instantiate (cloud, spawnPosition, spawnRotation);
				Debug.Log("Cloud Instantiated");
				yield return new WaitForSeconds (spawnWait);
			}


		}
	}
}
