﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkullSpawner : MonoBehaviour {
	
		public GameObject skull;
		public Vector3 spawnValues; 
		public int skullCount;
		public float spawnWait;
		public float startWait;
		public float waveWait;
		public Vector3 rotater; 



		void Start ()
		{
			StartCoroutine (SpawnSkulls ());
		}




	IEnumerator SpawnSkulls ()
	{	Debug.Log("SpawnSkullsCalled");
		yield return new WaitForSeconds (startWait);
		while (true)
		{
			for (int i = 0; i < skullCount; i++) 
			{	
				
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.Euler(rotater); 
				Instantiate (skull, spawnPosition, spawnRotation);
				Debug.Log("Skull Instantiated");
				yield return new WaitForSeconds (spawnWait);
			}


}
	}
}