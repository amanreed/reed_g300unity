﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	public GameObject hazard;
	public GameObject skull;
	public Vector3 spawnValues; 
	public int hazardCount; 
	public int skullCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;

	public GUIText scoreText;
	public GUIText restartText;
	public GUIText gameOverText;
	public GUIText winText;

	private bool gameOver;
	private bool restart; 
	public int score;

	void Start ()
	{
		gameOver = false;
		restart = false;
		restartText.text = "";
		gameOverText.text = "";
		//score = 0;
		//UpdateScore ();
		StartCoroutine (SpawnWaves ());
	}

	void Update ()
	{
		if (restart) { 
			if (Input.GetKeyDown (KeyCode.R)) {
				Application.LoadLevel (Application.loadedLevel);
			}
		}
	}


	IEnumerator SpawnWaves ()
	{
		yield return new WaitForSeconds (startWait);
		while (true)
		{
			for (int i = 0; i < hazardCount; i++) 
			{
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity; 
				Instantiate (hazard, spawnPosition, spawnRotation);
				Debug.Log("Hearts Placed");
				yield return new WaitForSeconds (spawnWait);
			}
/*	Works, but not the way I want it to		for (int i = 0; i < skullCount; i++) 
			{
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity; 
				Instantiate (skull, spawnPosition, spawnRotation);
				Debug.Log("Skull Placed");
				yield return new WaitForSeconds (spawnWait);
			}
			*/

			yield return new WaitForSeconds (waveWait);
			if (gameOver) {
				restartText.text = "Press 'R' for Restart";
				restart = true; 
				break; 
			}
		}
	
	
	}

/* Does not work	IEnumerator SpawnSkulls ()
	{	Debug.Log("SpawnSkullsCalled");
		yield return new WaitForSeconds (startWait);
		while (true)
		{
			for (int i = 0; i < skullCount; i++) 
			{	
				
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity; 
				Instantiate (skull, spawnPosition, spawnRotation);
				Debug.Log("Skull Instantiated");
				yield return new WaitForSeconds (spawnWait);
			}
			yield return new WaitForSeconds (waveWait);
					}


	}
*/









	//public void AddScore (int newScoreValue)
	//{
	//	score += newScoreValue;
	//	UpdateScore ();
	//}

	//void UpdateScore ()
	//{
	//	scoreText.text = "Score: " + score;
	//}
	public void GameOver ()
	{
		gameOverText.text = "Game Over!";
		gameOver = true;

	}

	public void GameWon ()
	{
		gameOverText.text = "You Won!";
	}
}

