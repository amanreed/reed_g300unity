﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestroyByContact : MonoBehaviour 
{
	public GameObject explosion;
	public GameObject playerExplosion;
	private int scoreValue;
	private GameController gameController;
	public float sizeChange;
	public float sizeChangeSmall;
	public int goodFoodCollected;
	public int lives;
	public int gameWonCondition; 
	public int gameLostCondition;
	public Image BurgerUI1;
	public Image BurgerUI2;
	public Image BurgerUI3;



	void Start ()
	{
		lives = 3;
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent <GameController> ();
		}
		if (gameController == null)
		{
			Debug.Log ("Cannot find 'GameController' script");

		}
	}

	void Update() {
		

	}


	void OnTriggerEnter(Collider other) 
	{
		print ("Triggered");
		if (other.tag == "Boundary")
			{ 
			print ("Boundary collision");
				return;
			}
		if (other.tag == "heart") {
			print ("Heart!");
			transform.localScale *= sizeChange;
			goodFoodCollected = goodFoodCollected + 1; 
			Debug.Log (goodFoodCollected);
			Destroy(other.gameObject);

			if (goodFoodCollected >= gameWonCondition) {
				Debug.Log ("Player wins");
				gameController.GameWon ();
				Destroy (gameObject);
			}
		}

		if (other.tag == "skull") {
			print ("Skull!");
			transform.localScale *= sizeChangeSmall;
			lives = lives - 1;
			Debug.Log ("I have fewer lives");
			Destroy(other.gameObject);

			if (lives == 2) {
				BurgerUI3.enabled = false;

			}
			if (lives == 1) {
				BurgerUI2.enabled = false;
			}

			if (lives <= 0) {
				BurgerUI1.enabled = false;
				Debug.Log ("I'm too small!");
				gameController.GameOver ();
				Destroy (gameObject);
			}
		}

		if (other.tag == "cloud") {


		}

//		Instantiate (explosion, transform.position, transform.rotation);
//		if (other.tag == "Player") {
//			Instantiate (playerExplosion, other.transform.position, other.transform.rotation);
//			gameController.GameOver ();
//		}
//		gameController.AddScore (scoreValue);
//		Destroy(other.gameObject);
//		Destroy(gameObject);
		}
		

}
